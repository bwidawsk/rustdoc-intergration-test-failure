use common::helper_library;
use rustdoc_test_repro;

mod common;
pub fn main() {
    helper_library();
    rustdoc_test_repro::add(4, 4);
}
